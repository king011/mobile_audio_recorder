import 'dart:async';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter/services.dart';

/// Recorder status
enum Status {
  Starting,
  Started,
  Stopping,
  Stoped,
}

/// Record aac
const FormatAAC = 'aac';

/// Record mp3
const FormatMP3 = 'mp3';

/// Record pcm
const FormatPCM = 'pcm';

/// Record pcm mono
const ChannelInMono = 1;

/// Record pcm stereo
const ChannelInStereo = 2;

/// pcm 16bit (little endian)
const EncodingPcm16Bit = 1;

/// pcm 8bit
const EncodingPcm8Bit = 2;

/// EventChannel Wrapper
class EventChannelWrapper {
  final _channel =
      const EventChannel('gitlab.com/king011/mobile_audio_recorder');
  dynamic _arguments;
  Stream<Uint8List> _stream;
  Completer<dynamic> _completer;

  /// listen from channel
  Future<StreamSubscription<Uint8List>> listen(
    Function onData,
    dynamic arguments,
  ) async {
    // 等待上次操作完成
    while (_completer != null) {
      await _completer.future;
    }
    // 鎖定操作
    final completer = Completer<dynamic>();
    _completer = completer;

    // 開始
    if (_stream == null) {
      _stream =
          _receiveBroadcastStream().map((event) => Uint8List.fromList(event));
    }
    // 設置參數
    _arguments = arguments;

    // 訂閱
    final subscription = _stream.listen((data) {
      if (_completer != null) {
        _completer.complete(null);
        _completer = null;
      }
      if (data?.isNotEmpty ?? false) {
        onData(data);
      }
    }, onError: (e) {
      if (_completer != null) {
        _completer.complete(e);
        _completer = null;
      }
    }, onDone: () {
      if (_completer != null) {
        _completer.complete(null);
        _completer = null;
      }
    });
    final exception = await completer.future;
    if (exception != null) {
      subscription?.cancel();
      throw exception;
    }
    return subscription;
  }

  Stream<dynamic> _receiveBroadcastStream() {
    final channel = _channel;
    final MethodChannel methodChannel =
        MethodChannel(channel.name, channel.codec);
    StreamController<dynamic> controller;
    controller = StreamController<dynamic>.broadcast(onListen: () async {
      channel.binaryMessenger.setMessageHandler(channel.name,
          (ByteData reply) async {
        if (reply == null) {
          controller.close();
        } else {
          try {
            final data = channel.codec.decodeEnvelope(reply);
            controller.add(data);
          } on PlatformException catch (e) {
            controller.addError(e);
          }
        }
        return null;
      });
      try {
        await methodChannel.invokeMethod<void>('listen', _arguments);
      } catch (exception, stack) {
        FlutterError.reportError(FlutterErrorDetails(
          exception: exception,
          stack: stack,
          library: 'services library',
          context: ErrorDescription(
              'while activating platform stream on channel ${channel.name}'),
        ));
      }
    }, onCancel: () async {
      channel.binaryMessenger.setMessageHandler(channel.name, null);
      try {
        await methodChannel.invokeMethod<void>('cancel', _arguments);
      } catch (exception, stack) {
        FlutterError.reportError(FlutterErrorDetails(
          exception: exception,
          stack: stack,
          library: 'services library',
          context: ErrorDescription(
              'while de-activating platform stream on channel ${channel.name}'),
        ));
      }
    });
    return controller.stream;
  }
}

/// SampleRate and BitRate range
class SampleBitRate {
  final int sampleRate;
  final int maxBitRate;
  final int minBitRate;
  const SampleBitRate({this.sampleRate, this.maxBitRate, this.minBitRate});
}

/// Recorder Single
class MobileAudioRecorder {
  static Status _status = Status.Stoped;

  /// Recorder status
  static Status get status => _status;

  /// if stoped return true
  static bool get isStoped => _status == Status.Stoped;

  /// if started return true
  static bool get isStarted => _status == Status.Started;

  // ignore: close_sinks
  static StreamController<Status> _controller =
      StreamController<Status>.broadcast();

  /// status stream
  static Stream<Status> get stream => _controller.stream;

  /// aac/mp3 supported sample rate and bit rate
  static const List<SampleBitRate> sampleBitRate = const [
    const SampleBitRate(sampleRate: 8000, minBitRate: 16000, maxBitRate: 48000),
    const SampleBitRate(
        sampleRate: 11025, minBitRate: 16000, maxBitRate: 48000),
    const SampleBitRate(
        sampleRate: 12000, minBitRate: 24000, maxBitRate: 64000),
    const SampleBitRate(
        sampleRate: 16000, minBitRate: 24000, maxBitRate: 96000),
    const SampleBitRate(
        sampleRate: 22050, minBitRate: 32000, maxBitRate: 128000),
    const SampleBitRate(
        sampleRate: 24000, minBitRate: 32000, maxBitRate: 128000),
    const SampleBitRate(
        sampleRate: 32000, minBitRate: 48000, maxBitRate: 192000),
    const SampleBitRate(
        sampleRate: 44100, minBitRate: 64000, maxBitRate: 320000),
    const SampleBitRate(
        sampleRate: 48000, minBitRate: 64000, maxBitRate: 320000),
  ];

  /// supported output formats
  static List<String> get formats => const [FormatAAC, FormatMP3, FormatPCM];
  static final _channel = EventChannelWrapper();

  /// check phone permission
  static Future<bool> checkPermission() async =>
      Permission.microphone.request().isGranted;

  /// request phone permission
  static Future<void> requestPermission() async =>
      Permission.microphone.request();

  static StreamSubscription<Uint8List> _subscription;
  static _verify(int sampleRate, int bitRate) {
    for (var i = 0; i < sampleBitRate.length; i++) {
      final element = sampleBitRate[i];
      if (element.sampleRate != sampleRate) {
        continue;
      }

      if (bitRate < element.minBitRate || bitRate > element.maxBitRate) {
        throw Exception(
            'When sample rate is $sampleRate, the bit rate must rang [${element.minBitRate},${element.maxBitRate}]');
      }
      return;
    }
    throw Exception('Sample rate not support : $sampleRate');
  }

  /// start recording
  static Future<void> start({
    bool debug = false,
    String format = FormatAAC,
    int sampleRate = 44100,
    int bitRate = 96000,
    int channel = ChannelInMono,
    int quality = 2, // Lame encoding quality is only valid for mp3
    int audioFormat = EncodingPcm16Bit, // pcm data format
    @required Function(Uint8List) onData,
  }) async {
    assert(debug != null);
    assert(format != null);
    assert(sampleRate != null);
    assert(bitRate != null);
    assert(channel != null);
    assert(quality != null);
    assert(onData != null);

    // 驗證 參數
    if (channel != ChannelInMono && channel != ChannelInStereo) {
      throw Exception('channel not supported');
    }
    if (audioFormat != EncodingPcm16Bit && audioFormat != EncodingPcm8Bit) {
      throw Exception('audio format not supported');
    }
    switch (format) {
      case FormatAAC:
      case FormatMP3:
        _verify(sampleRate, bitRate);
        break;
    }

    // 驗證狀態
    if (_status != Status.Stoped) {
      throw Exception('recorder not stopped');
    }
    _status = Status.Starting;
    _controller.add(_status);
    try {
      await _start(
        arguments: {
          'debug': debug,
          'format': format,
          'sampleRate': sampleRate,
          'bitRate': bitRate,
          'channel': channel,
          'quality': quality,
          'audioFormat': audioFormat,
        },
        onData: onData,
      );
      _status = Status.Started;
      _controller.add(_status);
    } catch (e) {
      _status = Status.Stoped;
      _controller.add(_status);
      throw e;
    }
  }

  static _start({
    @required dynamic arguments,
    @required Function onData,
  }) async {
    bool granted = await checkPermission();
    if (!granted) {
      // 請求權限後 重試
      await requestPermission();

      granted = await checkPermission();
    }
    if (!granted) {
      throw Exception('Permission Denied');
    }
    // 監聽數據
    _subscription = await _channel.listen(onData, arguments);
  }

  /// stop recording
  static Future<void> stop() async {
    // 驗證狀態
    if (_status != Status.Started) {
      throw Exception('recorder not stopped');
    }
    _status = Status.Stopping;
    _controller.add(_status);
    try {
      if (_subscription != null) {
        _subscription.cancel();
        _subscription = null;
      }
      _status = Status.Stoped;
      _controller.add(_status);
    } catch (e) {
      _status = Status.Started;
      _controller.add(_status);
      throw e;
    }
  }
}
