## 1.0.0+4
* support 8bit pcm

## 1.0.0+3
* support ChannelInStereo

## 1.0.0+2
* support set sampleRate and bitRate
* mp3 support set quality

## 1.0.0+1

* add BSD LICENSE

## 1.0.0

* support mp3

## 0.0.3

* support aac
* support pcm

## 0.0.1

* TODO: Describe initial release.
