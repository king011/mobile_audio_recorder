# mobile_audio_recorder

This is a flutter plugin for recording audio for Android only. Not support IOS. You can read the samples from the microphone .

Current features:
* Support format aac (default)
* Support format mp3
* Support format pcm 


Limitations:
* Only support 16bit/8bit Mono/Stereo (for now)
* Only support Android (for now)

# Permissions

Make sure you have microphone recording permissions enabled for your project. To do so, add this line to the AndroidManifest.xml:

```
<uses-permission android:name="android.permission.RECORD_AUDIO"/>
```

# AAC/MP3 SampleRate and BitRate

| SampleRate | BitRate |
| -------- | -------- |
| 8000     | 16000 ~ 48000     |
| 11025     | 16000 ~ 48000     |
| 12000     | 24000 ~ 64000     |
| 16000     | 24000 ~ 96000     |
| 22050     | 32000 ~ 128000     |
| 24000     | 32000 ~ 128000     |
| 32000     | 48000 ~ 192000     |
| 44100     | 64000 ~ 320000     |
| 48000     | 64000 ~ 320000     |

# Example:

```
    // start recording
    await MobileAudioRecorder.start(
        format: FormatAAC,
        sampleRate: 44100,
        sampleRate: 96000,
        channel: ChannelInMono ,// ChannelInStereo
        audioFormat: EncodingPcm16Bit  ,// EncodingPcm8Bit,
        onData: (Uint8List buffer) {
            // do something
        },
    );

    Future.delayed(Duration(seconds: 2)).then((value) {
        // stop recording
        MobileAudioRecorder.stop();
    });
```