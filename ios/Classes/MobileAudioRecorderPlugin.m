#import "MobileAudioRecorderPlugin.h"
#if __has_include(<mobile_audio_recorder/mobile_audio_recorder-Swift.h>)
#import <mobile_audio_recorder/mobile_audio_recorder-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "mobile_audio_recorder-Swift.h"
#endif

@implementation MobileAudioRecorderPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftMobileAudioRecorderPlugin registerWithRegistrar:registrar];
}
@end
