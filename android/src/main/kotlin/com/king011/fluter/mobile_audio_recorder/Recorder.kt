package com.king011.fluter.mobile_audio_recorder

import android.media.AudioFormat
import android.media.AudioRecord
import android.media.MediaRecorder
import io.flutter.plugin.common.EventChannel.EventSink
import java.lang.Exception

interface Recorder {
    fun start()
    fun stop()
}

fun getAudioFormat(audioFormat: Int): String {
    return if (audioFormat == AudioFormat.ENCODING_PCM_8BIT) {
        "8bit"
    } else {
        "16bit"
    }
}

fun getBufferSize(sampleRate: Int, channel: Int, audioFormat: Int, eventSink: EventSink?): Int? {
    var channelConfig = if (channel == 2) {
        AudioFormat.CHANNEL_IN_STEREO
    } else {
        AudioFormat.CHANNEL_IN_MONO
    }
    try {
        return AudioRecord.getMinBufferSize(sampleRate,
                channelConfig,
                audioFormat)
    } catch (e: Exception) {
        eventSink!!.error("AudioRecord", "$e", null)
    }
    return null
}

fun createAudioRecord(sampleRate: Int, channel: Int, bufferSize: Int, audioFormat: Int, eventSink: EventSink?): AudioRecord? {
    var channelConfig = if (channel == 2) {
        AudioFormat.CHANNEL_IN_STEREO
    } else {
        AudioFormat.CHANNEL_IN_MONO
    }
    try {
        return AudioRecord(
                MediaRecorder.AudioSource.MIC,
                sampleRate,
                channelConfig,
                audioFormat,
                bufferSize)
    } catch (e: Exception) {
        eventSink!!.error("AudioRecord", "$e", null)
    }
    return null
}
