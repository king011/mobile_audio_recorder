package com.king011.fluter.mobile_audio_recorder

import android.media.AudioFormat
import android.media.AudioRecord
import android.os.Handler
import android.os.Looper
import android.os.Process
import android.util.Log
import io.flutter.plugin.common.EventChannel.EventSink
import java.nio.ByteBuffer
import java.nio.ByteOrder

class PcmRecorder : Recorder {
    private val logTag = "MobileAudioRecorderPlugin pcm"
    private var eventSink: EventSink? = null
    private var recording = false
    private var debug = false
    private var record: AudioRecord? = null
    private var audioBuffer: ShortArray? = null
    private var audioBuffer8bit: ByteArray? = null
    override fun start() {
        recording = true
        if (audioBuffer == null) {
            streamData(debug, record!!, audioBuffer8bit!!)
        } else {
            streamData(debug, record!!, audioBuffer!!)
        }
    }

    override fun stop() {
        recording = false
    }

    fun setOptions(debug: Boolean, sampleRate: Int, channel: Int, audioFormat: Int, eventSink: EventSink?): Boolean {
        if (debug) {
            Log.i(logTag, "audioFormat=${getAudioFormat(audioFormat)},channel=$channel,sampleRate=$sampleRate")
        }
        // 創建錄音機
        val bufferSize = getBufferSize(sampleRate, channel, audioFormat, eventSink) ?: return false
        val record = createAudioRecord(sampleRate, channel, bufferSize, audioFormat, eventSink)
                ?: return false
        if (record.state != AudioRecord.STATE_INITIALIZED) {
            Log.e(logTag, "Audio Record can't initialize!")
            eventSink!!.error("AudioRecord", "AudioRecord can't initialize!", null)
            return false
        }
        // 爲錄音 分配內存
        if (audioFormat == AudioFormat.ENCODING_PCM_8BIT) {
            audioBuffer8bit = ByteArray(bufferSize)
        } else {
            audioBuffer = ShortArray(bufferSize / 2)
        }

        this.record = record
        this.debug = debug
        this.eventSink = eventSink
        return true
    }

    private fun streamData(debug: Boolean, record: AudioRecord, audioBuffer: ShortArray) {
        Thread(Runnable {
            Process.setThreadPriority(Process.THREAD_PRIORITY_AUDIO)

            /** 開始錄音循環 */
            record.startRecording()
            while (recording) {
                val shortOut = record.read(audioBuffer, 0, audioBuffer.size)
                if (shortOut < 1) {
                    continue
                }
                val byteBuffer = ByteBuffer.allocate(shortOut * 2)
                byteBuffer.order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().put(audioBuffer.sliceArray(IntRange(0, shortOut - 1)))
                if (debug) {
                    Log.i(logTag, "post ${byteBuffer.array().size}")
                }
                Handler(Looper.getMainLooper()).post {
                    eventSink!!.success(byteBuffer.array())
                }
            }
            record.stop()
            record.release()
        }).start()
    }

    private fun streamData(debug: Boolean, record: AudioRecord, audioBuffer: ByteArray) {
        Thread(Runnable {
            Process.setThreadPriority(Process.THREAD_PRIORITY_AUDIO)

            /** 開始錄音循環 */
            record.startRecording()
            while (recording) {
                val shortOut = record.read(audioBuffer, 0, audioBuffer.size)
                if (shortOut < 1) {
                    continue
                }
                val byteBuffer = audioBuffer.sliceArray(IntRange(0, shortOut - 1)).clone()
                if (debug) {
                    Log.i(logTag, "post ${byteBuffer.size}")
                }
                Handler(Looper.getMainLooper()).post {
                    eventSink!!.success(byteBuffer)
                }
            }
            record.stop()
            record.release()
        }).start()
    }
}
