package com.king011.fluter.mobile_audio_recorder

import android.media.*
import android.media.MediaFormat.MIMETYPE_AUDIO_AAC
import android.os.Handler
import android.os.Looper
import android.os.Process
import android.util.Log
import io.flutter.plugin.common.EventChannel.EventSink
import java.nio.ByteBuffer
import java.nio.ByteOrder

class AacRecorder : Recorder {
    private val logTag = "MobileAudioRecorderPlugin aac"
    private var eventSink: EventSink? = null
    private var recording = false
    private var debug = false
    private var record: AudioRecord? = null
    private var audioBuffer: ShortArray? = null
    private var audioBuffer8bit: ByteArray? = null
    private var mediaCodec: MediaCodec? = null
    private var freqIdx = 0
    private var channel = 0
    override fun start() {
        recording = true
        if (audioBuffer == null) {
            streamData(debug, record!!, audioBuffer8bit!!, freqIdx, channel, mediaCodec!!)
        } else {
            streamData(debug, record!!, audioBuffer!!, freqIdx, channel, mediaCodec!!)
        }
    }

    override fun stop() {
        recording = false
    }

    fun setOptions(debug: Boolean, sampleRate: Int, channel: Int, bitRate: Int, audioFormat: Int, eventSink: EventSink?): Boolean {
        if (debug) {
            Log.i(logTag, "audioFormat=${getAudioFormat(audioFormat)},channel=$channel,sampleRate=$sampleRate,bitRate=$bitRate")
        }
        when (sampleRate) {
            96000 -> {
                freqIdx = 0x0
            }
            88200 -> {
                freqIdx = 0x1
            }
            64000 -> {
                freqIdx = 0x2
            }
            48000 -> {
                freqIdx = 0x3
            }
            44100 -> {
                freqIdx = 0x4
            }
            32000 -> {
                freqIdx = 0x5
            }
            24000 -> {
                freqIdx = 0x6
            }
            22050 -> {
                freqIdx = 0x7
            }
            16000 -> {
                freqIdx = 0x8
            }
            12000 -> {
                freqIdx = 0x9
            }
            11025 -> {
                freqIdx = 0xa
            }
            8000 -> {
                freqIdx = 0xb
            }
            7350 -> {
                freqIdx = 0xc
            }

            else -> {
                eventSink!!.error("SampleRate", "Not supported sample rate $sampleRate", null)
                return false
            }
        }

        // 創建錄音機
        val bufferSize = getBufferSize(sampleRate, channel, audioFormat, eventSink) ?: return false
        val record = createAudioRecord(sampleRate, channel, bufferSize, audioFormat, eventSink)
                ?: return false

        if (record.state != AudioRecord.STATE_INITIALIZED) {
            Log.e(logTag, "Audio Record can't initialize!")
            eventSink!!.error("AudioRecord", "AudioRecord can't initialize!", null)
            return false
        }
        // 爲錄音 分配內存
        if (audioFormat == AudioFormat.ENCODING_PCM_8BIT) {
            audioBuffer8bit = ByteArray(bufferSize)
        } else {
            audioBuffer = ShortArray(bufferSize / 2)
        }

        // 創建 aac 編碼器
        try {
            mediaCodec = createMediaCodec(sampleRate, bitRate, channel, audioFormat)
        } catch (e: Exception) {
            eventSink!!.error("MediaCodec", "$e", null)
            return false
        }
        this.record = record
        this.channel = channel
        this.debug = debug
        this.eventSink = eventSink
        return true
    }

    private fun streamData(debug: Boolean, record: AudioRecord, audioBuffer: ShortArray, freqIdx: Int, channel: Int, mediaCodec: MediaCodec) {
        Thread(Runnable {
            Process.setThreadPriority(Process.THREAD_PRIORITY_AUDIO)

            /** 開始錄音循環 */
            mediaCodec.start()
            record.startRecording()
            while (recording) {
                val shortOut = record.read(audioBuffer, 0, audioBuffer.size)
                if (shortOut < 1) {
                    continue
                }
                val buffer = ByteBuffer.allocate(shortOut * 2)
                buffer.order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().put(audioBuffer.sliceArray(IntRange(0, shortOut - 1)))

                putBuffer(debug, channel, mediaCodec, buffer)
            }
            record.stop()
            record.release()

            mediaCodec.stop()
            mediaCodec.release()
        }).start()
    }

    private fun putBuffer(debug: Boolean, channel: Int, mediaCodec: MediaCodec, buffer: ByteBuffer) {
        var offset = 0
        val size = buffer.limit()
        val bytes = buffer.array()
        while (offset != size) {
            val inputBufferIndex = mediaCodec.dequeueInputBuffer(-1)
            if (inputBufferIndex >= 0) {
                val inputBuffer = mediaCodec.getInputBuffer(inputBufferIndex)
                inputBuffer.clear()
                val inputLimit = inputBuffer.limit()
                var count = size - offset
                if (count > inputLimit) {
                    count = inputLimit
                }
                inputBuffer.put(bytes, offset, count)
                inputBuffer.limit(count)
                mediaCodec.queueInputBuffer(inputBufferIndex, 0, count, 0, 0)
                offset += count
            }
            val bufferInfo = MediaCodec.BufferInfo()
            var outputBufferIndex = mediaCodec.dequeueOutputBuffer(bufferInfo, 0)
            while (outputBufferIndex >= 0) {
                val outputBuffer = mediaCodec.getOutputBuffer(outputBufferIndex)
                outputBuffer.position(bufferInfo.offset)
                outputBuffer.limit(bufferInfo.offset + bufferInfo.size)
                val chunkAudio = ByteArray(bufferInfo.size + 7) // 7 is ADTS size
                addADTStoPacket(freqIdx, channel, chunkAudio, chunkAudio.size)
                outputBuffer.get(chunkAudio, 7, bufferInfo.size)
                outputBuffer.position(bufferInfo.offset)
                if (debug) {
                    Log.i(logTag, "post ${chunkAudio.size}")
                }
                Handler(Looper.getMainLooper()).post {
                    eventSink!!.success(chunkAudio)
                }
                mediaCodec.releaseOutputBuffer(outputBufferIndex, false)
                outputBufferIndex = mediaCodec.dequeueOutputBuffer(bufferInfo, 0)
            }
        }
    }

    private fun createMediaCodec(sampleRate: Int, bitRate: Int, channelCount: Int, audioFormat: Int): MediaCodec {
        val format = MediaFormat.createAudioFormat(MIMETYPE_AUDIO_AAC, sampleRate, channelCount)
        format.setInteger(MediaFormat.KEY_AAC_PROFILE, MediaCodecInfo.CodecProfileLevel.AACObjectLC)
        format.setInteger(MediaFormat.KEY_BIT_RATE, bitRate)
        format.setInteger(MediaFormat.KEY_PCM_ENCODING, audioFormat)

        val mediaCodec = MediaCodec.createEncoderByType(MIMETYPE_AUDIO_AAC)
        mediaCodec.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE)
        return mediaCodec
    }

    private fun addADTStoPacket(freqIdx: Int, chanCfg: Int, packet: ByteArray, packetLen: Int) {
        val profile = 2; // AAC LC

        // fill in ADTS data
        packet[0] = 0xFF.toByte()
        packet[1] = 0xF9.toByte()
        packet[2] = (((profile - 1) shl 6) + (freqIdx shl 2) + (chanCfg shr 2)).toByte()
        packet[3] = (((chanCfg and 3) shl 6) + (packetLen shr 11)).toByte()
        packet[4] = ((packetLen and 0x7FF) shr 3).toByte()
        packet[5] = (((packetLen and 7) shl 5) + 0x1F).toByte()
        packet[6] = 0xFC.toByte()
    }

    private fun streamData(debug: Boolean, record: AudioRecord, audioBuffer: ByteArray, freqIdx: Int, channel: Int, mediaCodec: MediaCodec) {
        Thread(Runnable {
            Process.setThreadPriority(Process.THREAD_PRIORITY_AUDIO)

            /** 開始錄音循環 */
            mediaCodec.start()
            record.startRecording()
            while (recording) {
                val shortOut = record.read(audioBuffer, 0, audioBuffer.size)
                if (shortOut < 1) {
                    continue
                }
                val buffer = ByteBuffer.allocate(shortOut)
                buffer.put(audioBuffer.sliceArray(IntRange(0, shortOut - 1)))

                putBuffer(debug, channel, mediaCodec, buffer)
            }
            record.stop()
            record.release()

            mediaCodec.stop()
            mediaCodec.release()
        }).start()
    }
}
