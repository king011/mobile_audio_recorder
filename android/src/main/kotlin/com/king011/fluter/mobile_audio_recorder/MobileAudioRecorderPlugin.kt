package com.king011.fluter.mobile_audio_recorder

import android.app.Activity
import android.content.pm.PackageManager
import android.media.*
import android.util.Log
import androidx.annotation.NonNull
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.EventChannel.EventSink
import io.flutter.plugin.common.PluginRegistry.Registrar
import io.flutter.plugin.common.PluginRegistry.RequestPermissionsResultListener
import java.util.*

/** MobileAudioRecorderPlugin */
public class MobileAudioRecorderPlugin : FlutterPlugin, RequestPermissionsResultListener, EventChannel.StreamHandler, ActivityAware {

    // / Constants
    private val logTag = "MobileAudioRecorderPlugin"
    private val signal = ByteArray(0)

    // / Variables (i.e. will change value)
    private var channel: EventChannel? = null
    private var eventSink: EventSink? = null
    private var recorder: Recorder? = null
    private var currentActivity: Activity? = null

    override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        val messenger = flutterPluginBinding.getFlutterEngine().getDartExecutor()

        channel = EventChannel(messenger, "gitlab.com/king011/mobile_audio_recorder")
        channel!!.setStreamHandler(MobileAudioRecorderPlugin())
    }

    companion object {
        @JvmStatic
        fun registerWith(registrar: Registrar) {
            val channel = EventChannel(registrar.messenger(), "gitlab.com/king011/mobile_audio_recorder")
            channel.setStreamHandler(MobileAudioRecorderPlugin())
        }
    }

    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        channel!!.setStreamHandler(null)
    }

    override fun onDetachedFromActivity() {
        currentActivity = null
    }

    override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
        currentActivity = binding.activity
        binding.addRequestPermissionsResultListener(this)
    }

    override fun onAttachedToActivity(binding: ActivityPluginBinding) {
        currentActivity = binding.activity
        binding.addRequestPermissionsResultListener(this)
    }

    override fun onDetachedFromActivityForConfigChanges() {
        currentActivity = null
    }

    /**
     * Called from Flutter, starts the stream.
     */
    override fun onListen(arguments: Any?, eventSink: EventSink?) {
        if (recorder != null) {
            recorder!!.stop()
            recorder = null
            return
        }

        val m = arguments as Map<String, Any>
        val debug = m["debug"] as Boolean
        val format = m["format"] as String
        val sampleRate = m["sampleRate"] as Int
        val bitRate = m["bitRate"] as Int
        val channel = m["channel"] as Int
        var audioFormat = if (m["audioFormat"] == 2) {
            AudioFormat.ENCODING_PCM_8BIT
        } else {
            AudioFormat.ENCODING_PCM_16BIT
        }
        when (format) {
            "pcm" -> {
                val pcm = PcmRecorder()
                if (!pcm.setOptions(debug, sampleRate, channel, audioFormat, eventSink)) {
                    return
                }
                recorder = pcm
            }
            "mp3" -> {
                val mp3 = Mp3Recorder()
                val quality = m["quality"] as Int
                if (!mp3.setOptions(debug, sampleRate, channel, bitRate, quality, audioFormat, eventSink)) {
                    return
                }
                recorder = mp3
            }
            "aac" -> {
                val aac = AacRecorder()
                if (!aac.setOptions(debug, sampleRate, channel, bitRate, audioFormat, eventSink)) {
                    return
                }
                recorder = aac
            }
            else -> { // 除非否則需要 else
                if (debug) {
                    Log.e(logTag, "format not supported,$format")
                }
                eventSink!!.error("Formart", "Format not supported", "Expect to get [aac, mp3, pcm], but get $format")
                return
            }
        }
        // 通知錄音開始
        eventSink!!.success(signal)
        // 開始錄音
        recorder!!.start()
        this.eventSink = eventSink
    }

    /**
     * Called from Flutter, which cancels the stream.
     */
    override fun onCancel(arguments: Any?) {
        if (recorder != null) {
            recorder!!.stop()
            recorder = null
        }
    }

    /**
     * Called by the plugin itself whenever it detects that permissions have not been granted.
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray): Boolean {
        val requestAudioPermissionCode = 200
        when (requestCode) {
            requestAudioPermissionCode -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) return true
        }
        return false
    }

//    private fun streamAacData() {
//        Thread(Runnable {
//            Process.setThreadPriority(Process.THREAD_PRIORITY_AUDIO)
//            val bufferSize = AudioRecord.getMinBufferSize(sampleRate, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT)
//            val audioBuffer = ShortArray(bufferSize / 2)
//            val record = AudioRecord(
//                    MediaRecorder.AudioSource.MIC,
//                    sampleRate,
//                    AudioFormat.CHANNEL_IN_MONO,
//                    AudioFormat.ENCODING_PCM_16BIT,
//                    bufferSize)
//            if (record.state != AudioRecord.STATE_INITIALIZED) {
//                Log.e(logTag, "Audio Record can't initialize!")
//                return@Runnable
//            }
//            val mMediaCodec = createAacMediaCodec()
//
//            /** 開始錄音循環 */
//            mMediaCodec.start()
//            val inputBuffers = mMediaCodec.getInputBuffers()
//            val outputBuffers = mMediaCodec.getOutputBuffers()
//            record.startRecording()
//            while (recording) {
//                val shortOut = record.read(audioBuffer, 0, audioBuffer.size)
//                if (shortOut < 1) {
//                    continue
//                }
//                val buffer = ByteBuffer.allocate(shortOut * 2)
//                buffer.order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().put(audioBuffer)
//
//                val inputBufferIndex = mMediaCodec.dequeueInputBuffer(-1)
//                if (inputBufferIndex >= 0) {
//                    val inputBuffer = inputBuffers[inputBufferIndex]
//                    inputBuffer.clear()
//                    inputBuffer.put(buffer)
//                    inputBuffer.limit(buffer.limit())
//                    mMediaCodec.queueInputBuffer(inputBufferIndex, 0, buffer.limit(), 0, 0)
//                }
//                val bufferInfo = MediaCodec.BufferInfo()
//                var outputBufferIndex = mMediaCodec.dequeueOutputBuffer(bufferInfo, 0)
//                while (outputBufferIndex >= 0) {
//                    val outputBuffer = outputBuffers[outputBufferIndex]
//                    outputBuffer.position(bufferInfo.offset)
//                    outputBuffer.limit(bufferInfo.offset + bufferInfo.size)
//                    val chunkAudio = ByteArray(bufferInfo.size + 7) // 7 is ADTS size
//                    addADTStoPacket(chunkAudio, chunkAudio.size)
//                    outputBuffer.get(chunkAudio, 7, bufferInfo.size)
//                    outputBuffer.position(bufferInfo.offset)
//                    Handler(Looper.getMainLooper()).post {
//                        eventSink!!.success(chunkAudio)
//                    }
//                    mMediaCodec.releaseOutputBuffer(outputBufferIndex, false)
//                    outputBufferIndex = mMediaCodec.dequeueOutputBuffer(bufferInfo, 0)
//                }
//            }
//            record.stop()
//            record.release()
//            mMediaCodec.stop()
//            mMediaCodec.release()
//        }).start()
//    }

//
}
