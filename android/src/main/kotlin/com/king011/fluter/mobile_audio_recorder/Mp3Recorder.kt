package com.king011.fluter.mobile_audio_recorder

import android.media.AudioFormat
import android.media.AudioRecord
import android.os.Handler
import android.os.Looper
import android.os.Process
import android.util.Log
import com.czt.mp3recorder.util.LameUtil
import io.flutter.plugin.common.EventChannel.EventSink

class Mp3Recorder : Recorder {
    private val logTag = "MobileAudioRecorderPlugin mp3"
    private var eventSink: EventSink? = null
    private var recording = false
    private var debug = false
    private var record: AudioRecord? = null
    private var audioBuffer: ShortArray? = null
    private var audioBuffer8bit: ByteArray? = null
    private var mp3Buffer: ByteArray? = null
    private var channel = 0
    override fun start() {
        recording = true
        if (audioBuffer == null) {
            streamData(debug, record!!, audioBuffer8bit!!, mp3Buffer!!, channel)
        } else {
            streamData(debug, record!!, audioBuffer!!, mp3Buffer!!, channel)
        }
    }

    override fun stop() {
        recording = false
    }

    fun setOptions(debug: Boolean, sampleRate: Int, channel: Int, bitRate: Int, quality: Int, audioFormat: Int, eventSink: EventSink?): Boolean {
        if (debug) {
            Log.i(logTag, "audioFormat=${getAudioFormat(audioFormat)},channel=$channel,sampleRate=$sampleRate,bitRate=$bitRate")
        }
        // 創建錄音機
        val bufferSize = getBufferSize(sampleRate, channel, audioFormat, eventSink) ?: return false
        val record = createAudioRecord(sampleRate, channel, bufferSize, audioFormat, eventSink)
                ?: return false
        if (record.state != AudioRecord.STATE_INITIALIZED) {
            Log.e(logTag, "Audio Record can't initialize!")
            eventSink!!.error("AudioRecord", "AudioRecord can't initialize!", null)
            return false
        }
        // 爲錄音 分配內存
        if (audioFormat == AudioFormat.ENCODING_PCM_8BIT) {
            audioBuffer8bit = ByteArray(bufferSize)
        } else {
            audioBuffer = ShortArray(bufferSize / 2)
        }
        // 初始化 mp3 編碼器
        LameUtil.init(sampleRate, channel, sampleRate, bitRate / 1000, quality)
        mp3Buffer = if (audioFormat == AudioFormat.ENCODING_PCM_8BIT) {
            ByteArray((7200 + bufferSize * 2 * 1.25).toInt())
        } else {
            ByteArray((7200 + bufferSize * 1.25).toInt())
        }

        this.record = record
        this.channel = channel
        this.debug = debug
        this.eventSink = eventSink
        return true
    }

    private fun streamData(debug: Boolean, record: AudioRecord, audioBuffer: ShortArray, mp3Buffer: ByteArray, channel: Int) {
        Thread(Runnable {
            Process.setThreadPriority(Process.THREAD_PRIORITY_AUDIO)

            /** 開始錄音循環 */
            record.startRecording()
            while (recording) {
                val shortOut = record.read(audioBuffer, 0, audioBuffer.size)
                if (shortOut < 1) {
                    continue
                }
                val input = audioBuffer.sliceArray(IntRange(0, shortOut - 1))
                var size: Int
                if (channel == 2) {
                    val count = shortOut / 2
                    val l = ShortArray(count)
                    var r = input
                    var i = 0
                    while (i < count) {
                        l[i] = input[i * 2]
                        r[i] = input[i * 2 + 1]
                        i++
                    }
                    r = r.sliceArray(IntRange(0, count))
                    size = LameUtil.encode(l, r, count, mp3Buffer)
                } else {
                    size = LameUtil.encode(input, input, input.size, mp3Buffer)
                }
                if (size > 0) {
                    val chunkAudio = mp3Buffer.sliceArray(IntRange(0, size - 1)).clone()
                    if (debug) {
                        Log.i(logTag, "post ${chunkAudio.size}")
                    }
                    Handler(Looper.getMainLooper()).post {
                        eventSink!!.success(chunkAudio)
                    }
                } else if (debug) {
                    Log.w(logTag, "LameUtil.encode $size")
                }
            }
            record.stop()
            record.release()
//            // flush
//            val size = LameUtil.flush(mp3Buffer)
//            if (size > 0) {
//                Log.i(logTag, size.toString())
//
//                val chunkAudio = mp3Buffer.sliceArray(IntRange(0, size))
//                Handler(Looper.getMainLooper()).post {
//                    eventSink!!.success(chunkAudio)
//                }
//            }
            // close
            LameUtil.close()
        }).start()
    }

    private fun to16bit(v: Byte): Short {
        var tmp: Int = (v - 0x80)
        return (tmp shl 8).toShort()
    }

    private fun streamData(debug: Boolean, record: AudioRecord, audioBuffer: ByteArray, mp3Buffer: ByteArray, channel: Int) {
        Thread(Runnable {
            Process.setThreadPriority(Process.THREAD_PRIORITY_AUDIO)

            /** 開始錄音循環 */
            record.startRecording()
            while (recording) {
                val shortOut = record.read(audioBuffer, 0, audioBuffer.size)
                if (shortOut < 1) {
                    continue
                }
                val input = audioBuffer.sliceArray(IntRange(0, shortOut - 1))
                var size: Int
                if (channel == 2) {
                    val count = shortOut / 2
                    val l = ShortArray(count)
                    var r = ShortArray(count)
                    var i = 0
                    while (i < count) {
                        l[i] = to16bit(input[i * 2])
                        r[i] = to16bit(input[i * 2 + 1])
                        i++
                    }
                    size = LameUtil.encode(l, r, count, mp3Buffer)
                } else {
                    val data = ShortArray(input.size)
                    var i = 0
                    while (i < data.size) {
                        data[i] = to16bit(input[i])
                        i++
                    }
                    size = LameUtil.encode(data, data, data.size, mp3Buffer)
                }
                if (size > 0) {
                    val chunkAudio = mp3Buffer.sliceArray(IntRange(0, size - 1)).clone()
                    if (debug) {
                        Log.i(logTag, "post ${chunkAudio.size}")
                    }
                    Handler(Looper.getMainLooper()).post {
                        eventSink!!.success(chunkAudio)
                    }
                } else if (debug) {
                    Log.w(logTag, "LameUtil.encode $size")
                }
            }
            record.stop()
            record.release()
//            // flush
//            val size = LameUtil.flush(mp3Buffer)
//            if (size > 0) {
//                Log.i(logTag, size.toString())
//
//                val chunkAudio = mp3Buffer.sliceArray(IntRange(0, size))
//                Handler(Looper.getMainLooper()).post {
//                    eventSink!!.success(chunkAudio)
//                }
//            }
            // close
            LameUtil.close()
        }).start()
    }
}
