import 'dart:io';

import 'package:flutter/material.dart';
import 'package:mobile_audio_recorder/mobile_audio_recorder.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _controller = TextEditingController(text: '192.168.251.50:8964');
  final _controllerSampleRate = TextEditingController(text: '44100');
  final _controllerBitRate = TextEditingController(text: '96000');
  bool _isWorking = false;
  bool get isWorking => _isWorking;
  bool get isNotWorking => !_isWorking;
  bool _isRecording = false;
  Socket _socket;
  dynamic _error;
  String _format = FormatAAC;
  bool _stereo = false;
  bool _bit8 = false;
  @override
  void initState() {
    super.initState();
    MobileAudioRecorder.stream.listen((event) {
      debugPrint('$event');
    });
  }

  @override
  void dispose() {
    _stop();
    super.dispose();
  }

  _start() async {
    setState(() {
      _isWorking = true;
      _error = null;
    });
    try {
      final sampleRate = int.parse(_controllerSampleRate.text.trim());
      final bitRate = int.parse(_controllerBitRate.text.trim());
      // connect server
      final address = _controller.text.trim();
      final index = address.indexOf(':');
      final host = address.substring(0, index);
      final port = int.parse(address.substring(index + 1));
      _socket = await Socket.connect(host, port);
      // start recording
      await MobileAudioRecorder.start(
        debug: true,
        format: _format,
        sampleRate: sampleRate,
        bitRate: bitRate,
        channel: _stereo ? ChannelInStereo : ChannelInMono,
        audioFormat: _bit8 ? EncodingPcm8Bit : EncodingPcm16Bit,
        onData: (buffer) {
          //    debugPrint('send ${buffer.length}');
          _socket?.add(buffer);
        },
      );
      setState(() {
        _isRecording = true;
        _isWorking = false;
      });
    } catch (e) {
      if (_socket != null) {
        _socket.close();
        _socket = null;
      }
      setState(() {
        _isWorking = false;
        _error = e;
      });
      debugPrint('$e');
    }
  }

  _stop() async {
    setState(() {
      _isWorking = true;
      _error = null;
    });
    try {
      await MobileAudioRecorder.stop();
      if (_socket != null) {
        _socket.close();
        _socket = null;
      }
      setState(() {
        _isWorking = false;
        _isRecording = false;
      });
    } catch (e) {
      setState(() {
        _isWorking = false;
        _error = e;
      });
      debugPrint('$e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('mobile recorder test'),
        ),
        body: ListView(
          children: <Widget>[
            TextFormField(
              readOnly: isWorking,
              controller: _controller,
              decoration: InputDecoration(
                labelText: 'server address',
                prefixIcon: Icon(Icons.network_cell),
              ),
            ),
            TextFormField(
              readOnly: isWorking || _isRecording,
              controller: _controllerSampleRate,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                labelText: "SampleRate",
                prefixIcon: Icon(Icons.audiotrack),
              ),
            ),
            TextFormField(
              enabled: _format != FormatPCM,
              readOnly: isWorking || _isRecording,
              controller: _controllerBitRate,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                labelText: "BitRate",
                prefixIcon: Icon(Icons.audiotrack),
              ),
            ),
            SwitchListTile(
              title: Text('channel in stereo'),
              value: _stereo,
              onChanged: (ok) {
                setState(() {
                  _stereo = ok;
                });
              },
            ),
            SwitchListTile(
              title: Text('pcm 8bit'),
              value: _bit8,
              onChanged: (ok) {
                setState(() {
                  _bit8 = ok;
                });
              },
            ),
            FlatButton(
              child: Text("start"),
              onPressed: isWorking ? null : _start,
            ),
            FlatButton(
              child: Text("stop"),
              onPressed: isWorking ? null : _stop,
            ),
            Icon(_isRecording ? Icons.mic : Icons.mic_off),
            Column(
              children: MobileAudioRecorder.formats
                  .map<Widget>(
                    (format) => RadioListTile(
                        title: Text(format),
                        value: format,
                        groupValue: _format,
                        onChanged: isWorking | _isRecording
                            ? null
                            : (val) {
                                setState(() {
                                  _format = val;
                                });
                              }),
                  )
                  .toList(),
            ),
            Offstage(
              offstage: _error == null,
              child: Container(
                padding: EdgeInsets.all(5),
                child: Text(
                  '$_error',
                  style: TextStyle(color: Theme.of(context).errorColor),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
